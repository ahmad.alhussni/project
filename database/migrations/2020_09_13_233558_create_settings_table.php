<?php

use App\Models\Setting;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('key');
            $table->longText('value')->nullable();
            $table->string('language');
            $table->timestamps();
        });


        Setting::updateOrCreate(
            [
                'language' => "en",
                'key' => "logo",
            ],
            [
                'value' => "logo.png"
            ]
        );

        Setting::updateOrCreate(
            [
                'language' => "en",
                'key' => "admin_logo",
            ],
            [
                'value' => "logo.png"
            ]
        );

        Setting::updateOrCreate(
            [
                'language' => "ar",
                'key' => "logo",
            ],
            [
                'value' => "logo.png"
            ]
        );

        Setting::updateOrCreate(
            [
                'language' => "ar",
                'key' => "admin_logo",
            ],
            [
                'value' => "logo.png"
            ]
        );

        Setting::updateOrCreate(
            [
                'language' => "en",
                'key' => "footer_logo ",
            ],
            [
                'value' => "logo.png"
            ]
        );

        Setting::updateOrCreate(
            [
                'language' => "ar",
                'key' => "footer_logo ",
            ],
            [
                'value' => "logo.png"
            ]
        );


        Setting::updateOrCreate(
            [
                'language' => "en",
                'key' => "name",
            ],
            [
                'value' => "English name"
            ]
        );

        Setting::updateOrCreate(
            [
                'language' => "ar",
                'key' => "name",
            ],
            [
                'value' => "الاسم بالعربية"
            ]
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
