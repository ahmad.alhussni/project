<?php

use App\Models\Category;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('title')->nullable();
            $table->string('type')->nullable()->default('main');
            $table->string('image')->nullable();
            $table->string('slug')->nullable();
            $table->unsignedBigInteger('company_id')->nullable()->default(0);
            $table->enum('status',['0','1'])->default('1');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
        Category::insert(['user_id' => '1','title' => 'title1']);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
