<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('country_id');
            $table->string('name');
            $table->string('country');
            $table->boolean('status')->nullable()->default(1);
            $table->foreign('country_id')->references('id')->on('city');
            $table->timestamps();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('city_id')->nullable();
            $table->string('image')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('mobile')->nullable();
            $table->string('logo')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('website')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('email')->nullable();
            $table->string('token')->nullable();
            $table->string('code')->nullable();
            $table->timestamp('mobile_verified_at')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('note')->nullable();
            $table->boolean('status')->nullable()->default(1);
            $table->foreign('city_id')->references('id')->on('city');
            $table->rememberToken();
            $table->timestamps();
        });

        User::insert(['first_name' => 'Admin' ,'last_name' => 'Panel' , 'mobile' => '970599883621' , 'email' => 'admin@email.com', 'email_verified_at' => date('Y-m-d H:i:s') ,'city_id'=>null,'password' => Hash::make('123456'),'created_at' => date('Y-m-d H:i:s')]);
        $user = User::find(1);

        $Admin = Role::findOrCreate('Admin');
        $Vendor = Role::findOrCreate('Vendor');

        $user->syncRoles($Admin->id);
        foreach (User::perms() as $permission) {
            Permission::findOrCreate($permission);
            $Admin->givePermissionTo($permission);
        }
        // set_vendor_permistion
        $Vendor->givePermissionTo("post management");
        $Vendor->givePermissionTo("settings edit");
        $Vendor->givePermissionTo("payments_details management");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city');
        Schema::dropIfExists('users');
    }
}
