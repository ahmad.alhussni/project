<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Activitylog\Traits\CausesActivity;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles, HasApiTokens, Notifiable, LogsActivity, CausesActivity;

    protected $fillable = [
        'first_name',
        'last_name',
        'mobile',
        'email',
        'image',
        'password',
        'note',
        'status',
        'logo',
        'company_name',
        'company_description',
        'video_360',
        'delivery',
        'phone',
        'whatsapp',
        'website',
        'facebook',
        'instagram',
        'consultancies',
    ];
    protected $appends = ['name'];

    protected static $logFillable = true;
    protected static $submitEmptyLogs = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'token', 'code'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getImageAttribute($val)
    {
        return ($val != null) ? asset('storage/' . $val) : asset("storage/profile.png");
    }

    public function getBannerAttribute($val)
    {
        return ($val != null) ? asset('storage/' . $val) : asset("storage/banner.png");
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function carts()
    {
        return $this->hasMany(Cart::class);
    }

    public function messages()
    {
        return $this->hasMany(Contact::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function checkCart($id){

        if(auth()->check()) {
            $carts = Cart::where('parent_id',$id)->where('user_id',auth()->id())->get();
            return $carts->count() > 0;
        }
        return false;
    }

    public function favoritePosts()
    {
        return $this->belongsToMany(Post::class, 'favourites')->using(Favourite::class);
    }

    public function favoriteCompanies()
    {
        return $this->belongsToMany(Category::class, 'favourites',
            'user_id', 'company_id')->using(Favourite::class);
    }

    public function favoriteTraders()
    {
        return $this->belongsToMany(User::class, 'favourites',
            'user_id', 'trad_id')->using(Favourite::class);
    }

    public function traders()
    {
        return $this->belongsToMany(User::class, 'favourites')->using(Favourite::class);
    }

    static function perms()
    {
        return [
            'users management',
            'roles management',
            'sliders management',
            'categories management',
            'post management',
            'payments management',
            'payments_details management',
            'rates management',
            'contacts management',
            'service management',
            'company management',
            'userlog management',
            'favourite management',
            'city management',
            'settings edit'
        ];
    }

}
