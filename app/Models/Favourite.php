<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Spatie\Activitylog\Traits\LogsActivity;

class Favourite extends Pivot
{
    public $incrementing = true;
    protected $table = 'favourites';
    protected $fillable = ['user_id','post_id','trad_id','company_id'];
    use LogsActivity;
}
