<?php

namespace App\Models;

use App\Models\Category;
use App\Models\Favourite;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\CausesActivity;
use Spatie\Activitylog\Traits\LogsActivity;

class Post extends Model
{
    use LogsActivity, CausesActivity;

    protected $fillable = [
        'user_id',
        'category_id',
        'title',
        'description',
        'status'
    ];

    protected static $logAttributes = ['title', 'description', 'status'];
    protected static $submitEmptyLogs = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'favourites')->using(Favourite::class);
    }

    public function postImages()
    {
        return $this->hasMany(PostImage::class);
    }
}
