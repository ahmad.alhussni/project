<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\CausesActivity;
use Spatie\Activitylog\Traits\LogsActivity;

class Rate extends Model
{
    use LogsActivity, CausesActivity;
    protected $fillable = [
        'value','comment','user_id'
    ];

    protected static $logAttributes = ['value','comment'];
    protected static $submitEmptyLogs = false;

    public function user(){
        return $this->belongsTo(User::class);
    }

 }
