<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function __construct()
    {
//        $this->middleware(['permission:personal inforamtion']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = User::findOrFail($id);
        $users = User::get();
        if (request()->is('api/*')) {
            return response()->json(['item' => $item,'users' => $users]);
        } else {
            return view('vendor.users.edit', compact('item','users'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = User::findOrFail($id);

        $validate = [];
        if ($request->has('first_name')) {
            $validate['first_name'] = $request->first_name;
        }

        if ($request->has('last_name')) {
            $validate['last_name'] = $request->last_name;
        }
        if ($request->has('mobile')) {
            $validate['mobile'] = $request->mobile;
        }
        if ($request->has('email')) {
            $validate['email'] = $request->email;
        }
        if ($request->has('image')) {
            $validate['image'] = $request->image->store('users');
        }
        if ($request->has('password')) {
            $validate['password'] = Hash::make($request->password);
        }
        if ($request->has('note')) {
            $validate['note'] = $request->note;
        }
        if ($request->status >= "0") {
            $validate['status'] = $request->status;
        }
        /* update social media */
        if ($request->has('logo')) {
            $validate['logo'] = $request->image->store('logos');
        }


        if ($request->has('whatsapp')) {
            $validate['whatsapp'] = $request->whatsapp;
        }

        if ($request->has('website')) {
            $validate['website'] = $request->website;
        }

        if ($request->has('facebook')) {
            $validate['facebook'] = $request->facebook;
        }

        if ($request->has('instagram')) {
            $validate['instagram'] = $request->instagram;
        }

        $item->update($validate);
        if (request()->is('api/*')) {
            return response()->json($item);
        } else {
            return redirect()->route('vendor.home')->with(['success' => __("User updated done")]);
        }
    }


}
