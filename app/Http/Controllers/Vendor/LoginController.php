<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function login()
    {
        return view('vendor.login');
    }

    public function loginPost()
    {
        if (auth()->attempt(['email' => request()->input('email'), 'password' => request()->input('password')])) {

            if(auth()->user()->hasRole('Vendor')){
                return redirect(route('vendor.home'));
            }
            Auth::logout();
        }
        return redirect()->back()->with(['error' => 'خطأ في اسم المستخد او كلة المرور']);
    }

    public function logout()
    {
        if (Auth::check()) {
            Auth::logout();
            return redirect()->route('vendor.login');
        } else {
            return redirect()->back();
        }
    }

}

