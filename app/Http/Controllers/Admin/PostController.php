<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use App\Models\PostImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::latest()->paginate(10);
        if (Request::capture()->expectsJson()) {
            return response()->json($posts);
        } else {
            return view('admin.posts.index', ['items' => $posts]);
        }
    }

    public function create()
    {
        $categories = Category::where('status', '1')->get();
        return view('admin.posts.add', compact('categories'));
    }

    public function store()
    {
        request()->merge(['user_id' => Auth::id()]);
        request()->validate([
            'title' => 'required|string',
            'description' => 'required|string',
            'category_id' => 'required',
        ]);
        $post = Post::create(request()->all());
        foreach (request()->images as $image) {

            $img = $this->saveImage($image, $post->id);
            $post->postImages()->save($img);
        }

        if (Request::capture()->expectsJson()) {
            return response()->json($post);
        } else {
            return redirect()->route('admin.posts.index')->with(['success' => 'post Added Successfully']);
        }
    }

    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('admin.posts.view', ['post' => $post]);
    }

    public function edit($id)
    {
        $categories = Category::where('status', '1')->get();
        $post = Post::findOrFail($id);
        return view('admin.posts.edit', ['categories' => $categories, 'post' => $post]);
    }

    public function update($id)
    {
        request()->validate([
            'title' => 'string',
            'description' => 'string',
            'category_id' => 'String',
            'status'=>'numeric'
        ]);

        $post = Post::findOrFail($id);
        $post->update(request()->all());

        if (Request::capture()->expectsJson()) {
            return response()->json($post);
        } else {
            return redirect()->route('admin.posts.index')->with(['success' => 'post Updated Successfully']);
        }
    }

    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();
        return redirect()->route('admin.posts.index')->with(['success' => 'post Deleted Successfully']);
    }

    private function saveImage($image, $id)
    {
        $fileName = $image->store('/pictures/posts/' . $id);
        $postImage = new PostImage();
        $postImage->image_url = $fileName;
        return $postImage;
    }

}
