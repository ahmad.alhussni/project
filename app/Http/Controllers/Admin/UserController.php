<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:users management']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $items = User:: query();
       if(request('role_id')){
          $items->whereHas('roles',function ($q){
          return $q->where('id',request('role_id'));
         });
        }
        $items = $items->paginate(10);
        if (request()->is('api/*')) {
            return response()->json($items);
        } else {
            return view('admin.users.index', compact('items'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::get();
        return view('admin.users.add',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validate = $request->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'mobile' => 'required',
            'email' => '',
            'image' => 'image',
            'password' => 'required',
            'note' => '',
            'logo' => 'image',
            'whatsapp' => '',
            'website' => '',
            'facebook' => '',
            'instagram' => ''
        ]);

        $validate['image'] = $request->image ? $request->image->store('courses') : "images/logo.png";
        $validate['password'] = Hash::make($request->password);

        $item =  User::create($validate);


        $item->syncRoles( $request->role_id );


        return redirect()->route('admin.users.index')->with(['success' => __("User created done")]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = User::findOrFail($id);
        $users = User::get();
        if (request()->is('api/*')) {
            return response()->json(['item' => $item,'users' => $users]);
        } else {
            return view('admin.users.edit', compact('item','users'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = User::findOrFail($id);

        $validate = [];
        if ($request->has('first_name')) {
            $validate['first_name'] = $request->first_name;
        }

        if ($request->has('last_name')) {
            $validate['last_name'] = $request->last_name;
        }
        if ($request->has('mobile')) {
            $validate['mobile'] = $request->mobile;
        }
        if ($request->has('email')) {
            $validate['email'] = $request->email;
        }
        if ($request->has('image')) {
            $validate['image'] = $request->image->store('users');
        }
        if ($request->has('password')) {
            $validate['password'] = Hash::make($request->password);
        }
        if ($request->has('note')) {
            $validate['note'] = $request->note;
        }
        if ($request->status >= "0") {
            $validate['status'] = $request->status;
        }
        /* update social media */
        if ($request->has('logo')) {
            $validate['logo'] = $request->image->store('logos');
        }


        if ($request->has('whatsapp')) {
            $validate['whatsapp'] = $request->whatsapp;
        }

        if ($request->has('website')) {
            $validate['website'] = $request->website;
        }

        if ($request->has('facebook')) {
            $validate['facebook'] = $request->facebook;
        }

        if ($request->has('instagram')) {
            $validate['instagram'] = $request->instagram;
        }

        $item->update($validate);
        if (request()->is('api/*')) {
            return response()->json($item);
        } else {
            return redirect()->route('admin.users.index')->with(['success' => __("User updated done")]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id',$id)->delete();
        return redirect()->route('admin.users.index')->with(['success' => __("User deleted")]);
    }

}
