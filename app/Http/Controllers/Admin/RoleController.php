<?php

namespace App\Http\Controllers\Admin;


use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:roles management']);
    }

    /**
     * Display a listing of the Role.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $roles = new Role();
        $roles = $roles->get();
        return view('admin.roles.index', ['roles' => $roles]);
    }

    /**
     * Show the form for creating a new Role.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.roles.create');
    }

    /**
     * Store a newly created Role in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'name' => 'required',
        ]);

        $role = new Role();
        $role->name = request('name');
        $role->guard_name = "web";
        $role->Save();


        if($request->permissions) {
            foreach ($request->permissions as $permission) {
                Permission::findOrCreate($permission);
                $role->givePermissionTo($permission);
            }
        }

        return redirect()->route('admin.roles.index')->with('success', __('Roles saved successfully.'));
    }

    /**
     * Display the specified Role.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $role = Role::findOrFail($id);
        return view('admin.roles.show', ['role' => $role]);
    }

    /**
     * Show the form for editing the specified Role.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);

        return view('admin.roles.edit')->with('role', $role);
    }

    /**
     * Update the specified Role in storage.
     *
     * @param int $id
     * @param Request $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validate = $request->validate([
            'name' => 'required',
        ]);

        $role = Role::find($id);

        foreach ($role->permissions->whereNotIn('name', $request->permissions) as $permission) {
            $role->revokePermissionTo($permission);
        }

        if($request->permissions) {
            foreach ($request->permissions as $permission) {
                Permission::findOrCreate($permission);
                $role->givePermissionTo($permission);
            }
        }

        return redirect()->route('admin.roles.index')->with('success', __('Roles updated successfully.'));
    }

    /**
     * Remove the specified Role from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete($id);
        return redirect()->route('admin.roles.index')->with('success', __('Roles deleted successfully.'));
    }
}
