<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Spatie\Activitylog\Models\Activity;

class LoggingController extends Controller
{
    public function userActivities($id)
    {
        $user = User::findOrFail($id);
        return view('admin.users.activities',['item'=>$user]);
    }
    public function userActivity($id)
    {
        $item = Activity::find($id);
        return view('admin.users.activity',['item'=>$item]);
    }

}
