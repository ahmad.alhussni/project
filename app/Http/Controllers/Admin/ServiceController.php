<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:services management']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $items = Service::paginate(10);
        if (request()->is('api/*')) {
            return response()->json($items);
        } else {
            return view('admin.services.index', compact('items'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.services.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $items = Service::count();
        if($items >= 4){
            return  redirect()->back()->withErrors(['لا يمكنك اضافة خدمة جديدة عدد الخدمات 4 فقط']);
        }

        $request->validate([
            'name' => 'required|array',
            'description' => 'required|array',
            'image' => 'required|image'
        ]);


        $item =  Service::create([
            'image' => $request->image->store('services'),
            'slug' => Str::slug(request('name')['en'])
        ]);

        foreach (config('app.locales') as $locale) {
            $item->translates()->updateOrCreate(
                [
                    'language' => $locale,
                    'key' => 'name',
                ],
                [
                    'value' => request('name')[$locale] ? request('name')[$locale] : ""
                ]
            );

        }


        foreach (config('app.locales') as $locale) {
            $item->translates()->updateOrCreate(
                [
                    'language' => $locale,
                    'key' => 'description',
                ],
                [
                    'value' => request('description')[$locale] ? request('description')[$locale] : ""
                ]
            );

        }

        return redirect()->route('admin.services.index')->with(['success' => 'تم الحفظ بنجاح']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Service::findOrFail($id);

        if (request()->is('api/*')) {
            return response()->json(['item' => $item]);
        } else {
            return view('admin.services.edit', compact('item'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            //'image' => 'required|image'
        ]);


        $item = Service::findOrFail($id);
        $data = [];
        if ($request->has('image')) {
            $data['image'] = $request->image->store('services');
        }
        $item->update($data);

        foreach (config('app.locales') as $locale) {
            $item->translates()->updateOrCreate(
                [
                    'language' => $locale,
                    'key' => 'name',
                ],
                [
                    'value' => request('name')[$locale] ? request('name')[$locale] : ""
                ]
            );

        }


        foreach (config('app.locales') as $locale) {
            $item->translates()->updateOrCreate(
                [
                    'language' => $locale,
                    'key' => 'description',
                ],
                [
                    'value' => request('description')[$locale] ? request('description')[$locale] : ""
                ]
            );

        }


        if (request()->is('api/*')) {
            return response()->json($item);
        } else {
            return redirect()->route('admin.services.index')->with(['success' => 'تم ألتحديث بنجاح']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Service::where('id',$id)->delete();

        return redirect()->route('admin.services.index')->with(['success' => 'تم الحذف بنجاح']);
    }

}
