<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Rate;
use Illuminate\Support\Facades\Auth;

class RateController extends Controller
{

    public function index()
    {
        $rates = Rate::where('status', '=', '1')->paginate();

        return response()->json(['message' => 'success', "data" => $rates]);

    }

    public function store()
    {
        $userId = Auth::id();
        $rates = request()->validate([
            'value' => 'required|in:1,2,3,4,5',
            'comment' => 'nullable|string',
        ]);

        $rates['user_id'] = $userId;

        $rate = Rate::where('user_id', '=', $userId)->first();
        if ($rate == null)
            $rate = Rate::create($rates);
        else
            $rate->update($rates);

        return response()->json(['message' => 'success', "data" => $rate]);
    }

    public function update()
    {
        $rates = request()->validate([
            'value' => 'required|in:1,2,3,4,5',
            'comment' => 'nullable|string',
        ]);

        $rate = Rate::where('user_id', '=', Auth::id())->first();
        if ($rate != null)
            $rate->update($rates);

        return response()->json(['message' => 'success', "data" => $rate]);
    }

    public function delete($id)
    {
        $rate = Rate::findOrFail($id);
        $rate->delete();
        return response()->json(['message' => 'success', "data" => $rate]);
    }

    public function deActive($id)
    {
        $rate = Rate::findOrFail($id);
        $rate->update(['status' => '0']);
        return response()->json(['message' => 'success', "data" => $rate]);
    }

}

