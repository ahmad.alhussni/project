<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Banner;

class BannerController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {

        $banners = Banner::query();
        if(request('type')){
            $banners = $banners->where('type',request('type'));
        }
        $banners = $banners->get();
        return response()->json(['message' => 'success', "data" => $banners]);

    }
}
