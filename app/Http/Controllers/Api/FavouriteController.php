<?php

namespace App\Http\Controllers\Api;

use App\Models\Favourite;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FavouriteController extends Controller
{
    /*** Posts ***/

    function getFavoritePosts()
    {
        $user = Auth::user();
        $favoritePosts = $user->favoritePosts;
        return response()->json([
            'message' => 'success',
            'data' => ['posts' => $favoritePosts,],
        ]);
    }

    function getFavoritePost($id)
    {
        $user = Auth::user();
        $favoritePost = $user->favoritePosts()->wherePivot('post_id', '=', $id)->get();
        return response()->json([
            'message' => 'success',
            'data' => ['post' => $favoritePost,],
        ]);
    }

    function addPostToFavorites($id)
    {
        $userId = Auth::id();
        $favorite = Favourite::where([
            'user_id' => $userId,
            'post_id' => $id,
        ])->first();

        if ($favorite == null)
            $favorite = Favourite::create([
                'user_id' => $userId,
                'post_id' => $id,
            ]);

        return response()->json([
            'message' => 'Post Added To Favorites Successfully',
            'data' => ['favorite' => $favorite,],
        ]);
    }

    function removePostFromFavorites($id)
    {
        $favorite = Favourite::where([
            ['user_id', '=', Auth::id()],
            ['post_id', '=', $id],
        ]);
        $favorite->delete();
        return response()->json([
            'message' => 'Post Removed From Favorites Successfully',
        ]);
    }

    /*** Companies ***/

    function getFavoriteCompanies()
    {
        $user = Auth::user();
        $favoriteCompanies = $user->favoriteCompanies;
        return response()->json([
            'message' => 'success',
            'data' => ['companies' => $favoriteCompanies,],
        ]);
    }

    function getFavoriteCompany($id)
    {
        $user = Auth::user();
        $favoriteCompany = $user->favoriteCompanies()->wherePivot('company_id', '=', $id)->get();
        return response()->json([
            'message' => 'success',
            'data' => ['company' => $favoriteCompany,],
        ]);
    }

    function addCompanyToFavorites($id)
    {
        $userId = Auth::id();
        $favorite = Favourite::where([
            'user_id' => $userId,
            'company_id' => $id,
        ])->first();

        if ($favorite == null)
            $favorite = Favourite::create([
                'user_id' => $userId,
                'company_id' => $id,
            ]);

        return response()->json([
            'message' => 'Company Added To Favorites Successfully',
            'data' => ['favorite' => $favorite,]
        ]);
    }

    function removeCompanyFromFavorites($id)
    {
        $favorite = Favourite::where([
            ['user_id', '=', Auth::id()],
            ['company_id', '=', $id],
        ]);
        $favorite->delete();
        return response()->json([
            'message' => 'Company Removed From Favorites Successfully',
            'data' => ['favorite' => $favorite,]
        ]);
    }

    /*** Trades ***/

    function getFavoriteTrades()
    {
        $user = Auth::user();
        $favoriteTraders = $user->favoriteTraders;
        return response()->json([
            'message' => 'success',
            'data' => ['traders' => $favoriteTraders,],
        ]);
    }

    function getFavoriteTrade($id)
    {
        $user = Auth::user();
        $favoriteTrader = $user->favoriteTraders()->wherePivot('trad_id', '=', $id)->get();
        return response()->json([
            'message' => 'success',
            'data' => ['trader' => $favoriteTrader,]
        ]);
    }

    function addTradeToFavorites($id)
    {
        $userId = Auth::id();
        $favorite = Favourite::where([
            'user_id' => $userId,
            'trad_id' => $id,
        ])->first();

        if ($favorite == null)
            $favorite = Favourite::create([
                'user_id' => $userId,
                'trad_id' => $id,
            ]);
        return response()->json([
            'message' => 'Trader Added To Favorites Successfully',
            'data' => ['favorite' => $favorite,]

        ]);
    }

    function removeTradeFromFavorites($id)
    {
        $favorite = Favourite::where([
            ['user_id', '=', Auth::id()],
            ['trad_id', '=', $id],
        ]);
        $favorite->delete();
        return response()->json([
            'message' => 'Trader Removed From Favorites Successfully',
        ]);
    }

    /*** All Favorites ***/

    function index()
    {
        $user = Auth::user();
        return response()->json([
            'message' => 'success',
            'data' => [
                'posts' => $user->favoritePosts,
                'traders' => $user->favoriteTraders,
                'companies' => $user->favoriteCompanies
            ]
        ]);
    }
}
