<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Project;

class ProjectController extends Controller
{

    public function index()
    {

        $projects = Project::where('name',"Project")->paginate(10);

        return response()->json(['message' => 'success', "data" => $projects]);

    }

    public function show($id)
    {

        $project = Project::where('id',$id)->first();
        if(!$project){
            return response()->json(['message' => 'not exist']);
        }
        return response()->json(['message' => 'success', "data" => $project]);

    }


}

