<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Coupon;

class CouponController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {

        $coupon = Coupon::where('code',request('code'))->firstOrFail();
        return response()->json(['message' => 'success', "data" => $coupon]);

    }
}
