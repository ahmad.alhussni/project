<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Blog;

class BlogController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {

        $blogs = Blog::query();

        if (request('category_id')) {
            $blogs = $blogs->where('category_id', request('category_id'));
        }

        if (request('search')) {
            $blogs = $blogs->where('name', 'LIKE', '%%' . request('search') . '%%');
        }

        $blogs = $blogs->paginate(10);

        return response()->json(['message' => 'success', "data" => $blogs]);

    }
}
