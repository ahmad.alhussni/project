<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContactUsController extends Controller
{
    public function index()
    {
        $contacts= Contact::where('status','0')->paginate(10);
        return response()->json(['message' => 'success', 'data' => $contacts]);

    }

    public function store()
    {
        $contacts = request()->validate([
            'name' => 'required|string',
            'email' => 'required|email',
            'title' => 'required|string',
            'message' => 'required|string'
        ]);
        $userId = Auth::id();
        $contacts['user_id'] = $userId;

        $contact = Contact::create($contacts);

        return response()->json(['message' => 'success', "data" => $contact]);
    }
}
