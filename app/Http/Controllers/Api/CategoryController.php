<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;

class CategoryController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {

        $categories = Category::query();
        if(request('type')){
            $categories = $categories->where('type',request('type'));
        }
        $categories = $categories->get();

        return response()->json(['message' => 'success', "data" => $categories]);

    }
}
