<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Shipment;

class ShipmentController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {

        $shipments = Shipment::query();

        $shipments = $shipments->get();
        return response()->json(['message' => 'success', "data" => $shipments]);

    }
}
