<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
    function search()
    {
        $posts = $this->searchInPosts(request());
        $vendors = $this->searchInVendors(request());

        if ($posts->isEmpty() && $vendors->isEmpty())
            $message = 'No Result Found';
        else
            $message = 'Success';
        return response()->json([
            'results' => [
                'message' => $message,
                'data' => ['posts' => $posts,
                    'vendors' => $vendors,]
            ]
        ]);
    }

    function searchInPosts($request)
    {
        $data = $request->get('data');

        $posts = Post::where([
            ['status', '=', 1],
            ['title', 'like', "%{$data}%"],
        ])->orWhere([
            ['status', '=', 1],
            ['description', 'like', "%{$data}%"],
        ]);

        if (!$request->get('byLatest'))
            return $posts->get();
        else
            return $posts->orderBy('created_at')->get();

    }

    function searchInVendors($request)
    {
        $data = $request->get('data');
        $vendors = User::role('Vendor')
            ->where([
                ['status', '=', 1],
                ['first_name', 'like', "%{$data}%"],
            ])->orWhere([
                ['status', '=', 1],
                ['last_name', 'like', "%{$data}%"],
            ])->role('Vendor');

        if (!$request->get('byNearest'))
            return $vendors->get();
        else
            return $vendors->orderBy('first_name')->get();

    }
}
