<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['lang'])->group(function () {

    Route::post('register', 'Api\UserController@registerPost');
    Route::post('login', 'Api\UserController@loginPost');
    Route::post('reset-password', 'Api\UserController@resetPasswordPost');
    Route::post('reset-password-confirm', 'Api\UserController@resetPasswordConfirmPost');
    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::get('profile', 'Api\UserController@profile');
        Route::post('profile', 'Api\UserController@profilePost');
    });

    Route::get('sliders', 'Api\SliderController@index');
    Route::get('tags', 'Api\TagController@index');
    Route::get('categories', 'Api\CategoryController@index');
    Route::get('contacts', 'Api\ContactController@index');
    Route::post('contacts', 'Api\ContactController@store');
    Route::get('blogs', 'Api\BlogController@index');
    Route::get('partners', 'Api\PartnerController@index');
    Route::get('coupons', 'Api\CouponController@index');
    Route::get('shipments', 'Api\ShipmentController@index');
    Route::get('banners', 'Api\BannerController@index');
    Route::get('/rates', 'Api\RateController@index');
    Route::get('/designers', 'Api\DesignerController@index');
    Route::get('/designers/{id}', 'Api\DesignerController@show');
    Route::get('/contractors', 'Api\ContractorController@index');
    Route::get('/contractors/{id}', 'Api\ContractorController@show');
    Route::get('/furniture', 'Api\FurnitureController@index');
    Route::get('/furniture/{id}', 'Api\FurnitureController@show');
    Route::get('/projects', 'Api\ProjectController@index');
    Route::get('/projects/{id}', 'Api\ProjectController@show');

    Route::get('contact/us', 'Api\ContactUsController@index');
    Route::post('contact/us', 'Api\ContactUsController@store');

    Route::group(['middleware' => 'auth:sanctum'], function () {

        /*** Rate ***/
        Route::post('/rates', 'Api\RateController@store');
        Route::put('/rates', 'Api\RateController@update');
        Route::delete('/rates/{id}', 'Api\RateController@delete');
        Route::put('/rates/{id}', 'Api\RateController@deActive');

        Route::get('/carts', 'Api\CartController@index');
        Route::post('/carts', 'Api\CartController@store');
        Route::post('/carts/confirm', 'Api\CartController@confirm');
        Route::get('/orders', 'Api\OrderController@index');
        Route::get('/orders/{id}', 'Api\OrderController@show');

        /*** Search ***/
        Route::post('search', 'Api\SearchController@search');

        /*** Favorites ***/
        Route::get('favorites', 'Api\FavouriteController@index');
        Route::get('favorites/posts', 'Api\FavouriteController@getFavoritePosts');
        Route::get('favorites/posts/{id}', 'Api\FavouriteController@getFavoritePost');
        Route::post('favorites/posts/add/{id}', 'Api\FavouriteController@addPostToFavorites');
        Route::delete('favorites/postsremove/{id}', 'Api\FavouriteController@removePostFromFavorites');

        Route::get('favorites/companies', 'Api\FavouriteController@getFavoriteCompanies');
        Route::get('favorites/companies/{id}', 'Api\FavouriteController@getFavoriteCompany');
        Route::post('favorites/companies/add/{id}', 'Api\FavouriteController@addCompanyToFavorites');
        Route::delete('favorites/companies/remove/{id}', 'Api\FavouriteController@removeCompanyFromFavorites');

        Route::get('favorites/traders', 'Api\FavouriteController@getFavoriteTrades');
        Route::get('favorites/traders/{id}', 'Api\FavouriteController@getFavoriteTrade');
        Route::post('favorites/traders/add/{id}', 'Api\FavouriteController@addTradeToFavorites');
        Route::delete('favorites/traders/remove/{id}', 'Api\FavouriteController@removeTradeFromFavorites');

    });

});
