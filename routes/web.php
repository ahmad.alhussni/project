<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['lang'])->group(function () {

    // home page
    Route::get('/', 'HomeController@index')->name('home');
    // admin rout
    Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'], function () {
        Route::get('login', 'LoginController@login')->name('login');
        Route::post('login', 'LoginController@loginPost')->name('loginPost');
        Route::post('logout', 'LoginController@logout')->name('logout');
        // admin check
        Route::group(['middleware' => ['auth', 'AdminCheck']], function () {
            Route::get('/', 'DashboardController@dashboard')->name('home');
            Route::get('settings', 'DashboardController@settings')->name('settings');
            Route::post('settings', 'DashboardController@settingsPost');
            Route::resources([
                'users' => 'UserController',
                'roles' => 'RoleController',
                'sliders' => 'SliderController',
                'categories' => 'CategoryController',
                'services' => 'ServiceController',
                'payments' => 'PaymentController',
                'rates' => 'RateController',
                'contacts' => 'ContactController',
                'posts' => 'PostController',
            ]);

            Route::get('activities/user/{id}', 'LoggingController@userActivities')->name('activities');
            Route::get('activities/{id}', 'LoggingController@userActivity')->name('show_activity');
    });
  });

    // vendor
    Route::group(['namespace' => 'Vendor', 'prefix' => 'vendor', 'as' => 'vendor.'], function () {
        Route::get('login', 'LoginController@login')->name('login');
        Route::post('login', 'LoginController@loginPost')->name('loginPost');;
        Route::post('logout', 'LoginController@logout')->name('logout');

        Route::group(['middleware' => ['auth', 'VendorCheck']], function () {
            Route::get('/', 'DashboardController@dashboard')->name('home');
            Route::get('settings', 'DashboardController@settings')->name('settings');
            Route::post('settings', 'DashboardController@settingsPost');
            Route::resources([
                'users' => 'UserController',
                'posts' => 'PostController',
            ]);
        });
    });

});
