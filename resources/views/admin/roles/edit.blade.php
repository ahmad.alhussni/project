@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{__('Edit roles')}}</h5>
        </div>

        <div class="card-body">
            <form action="{{route('admin.roles.update',$role->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="tab1">
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">{{__('Name')}}</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="name" value="{{$role->name}}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="tab-content">
                        <label class="col-form-label">{{__('Permissions')}}</label>
                        <div class="row">

                            @foreach(\App\Models\User::perms() as $key => $permission)
                                <div class="col-md-6">
                                    <div class="row">
                                            <div class="col-md-4">
                                                <label>
                                                    <input {{$role->hasPermissionTo($permission) ? 'checked' : ''}} type="checkbox" name="permissions[{{$permission}}]" value="{{$permission}}">
                                                    {{__($permission)}}
                                                </label>
                                            </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i>
                        </button>
                    </div>
            </form>
            </form>
        </div>
    </div>


@endsection


@section('js_code')

@endsection
