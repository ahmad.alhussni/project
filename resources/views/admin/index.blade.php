@extends('admin.layouts.app')
@section('title','Dashboard Admin')
@section('content')

    <!-- Quick stats boxes -->
    <div class="row">
        <div class="col-lg-4">

            <!-- Members online -->
            <div class="card bg-teal-400">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0">{{\App\Models\User::count()}}</h3>
                     </div>

                    <div>
                        {{__("All users")}}
                    </div>
                </div>

                <div class="container-fluid">
                    <div id="members-online"></div>
                </div>
            </div>
            <!-- /members online -->

        </div>
    </div>
    <!-- /quick stats boxes -->

@endsection
