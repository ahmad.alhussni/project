@extends('admin.layouts.app')
@section('title',__('Users'))
@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{__('Users Activities')}}</h4>
                </div>

                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">

                        @if($item->description == 'updated')

                            <table class="table table-striped table-bordered" style="table-layout:fixed;">
                                <tr>
                                    <th>Attribute</th>
                                    <th>From</th>
                                    <th>To</th>
                                </tr>
                                <tbody>
                                @foreach($item->changes['attributes'] as $key => $val)
                                    @if($item->changes['attributes'][$key] != null)
                                        <tr>
                                            <td>{{$key}}</td>
                                            <td>{{ $item->changes['old'][$key] }}</td>
                                            <td>{{ $item->changes['attributes'][$key]}}</td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>

                        @elseif($item->description == 'created')

                            <table class="table table-striped table-bordered" style="table-layout:fixed;">
                                <tr>
                                    <th>Attribute</th>
                                    <th>value</th>
                                </tr>
                                <tbody>
                                @foreach($item->changes['attributes'] as $key => $val)
                                    @if($item->changes['attributes'][$key] != null)
                                        <tr>
                                            <td>{{$key}}</td>
                                            <td>{{ $item->changes['attributes'][$key]}}</td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>

                        @elseif($item->description == 'deleted')

                            <table class="table table-striped table-bordered" style="table-layout:fixed;">
                                <tr>
                                    <th>Attribute</th>
                                    <th>value</th>
                                </tr>
                                <tbody>
                                @foreach($item->changes['attributes'] as $key => $val)
                                    @if($item->changes['attributes'][$key] != null)
                                        <tr>
                                            <td>{{$key}}</td>
                                            <td>{{ $item->changes['attributes'][$key]}}</td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
