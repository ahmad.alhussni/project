@extends('admin.layouts.app')
@section('title',__('Users'))
@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{__('Users Activities')}}</h4>
                </div>

                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                        @if($item->actions->count() > 0)
                            <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>{{__('Id')}}</th>
                                        <th>{{__('Activity Type')}}</th>
                                        <th>{{__('Create Date')}}</th>
                                        <th>{{__('Update Date')}}</th>
                                        <th>{{__('Actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($item->actions as $item)
                                        <tr>
                                            <td>{{$item->id}}</td>
                                            <td>{{$item->subject_type}} has been {{$item->description}}</td>
                                            <td>{{$item->created_at}}</td>
                                            <td>{{$item->updated_at}}</td>
                                            <td>
                                                <button class="btn btn-primary">
                                                    <a href="{{route('admin.show_activity',$item->id)}}">
                                                        <i class="icon-eye" style="color: white"></i>
                                                    </a>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <h4 class="alert alert-danger text-center"> {{__('There is no items')}}</h4>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
