@extends('admin.layouts.app')
@section('title',__('Posts'))
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{__('Post')}}</h4>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                        <div class="row m-4">
                            @foreach($post->postImages as $image)
                                <div class="col-md-auto m-1">
                                    <img style="width: 300px; height: 300px;"
                                         src="{{asset('storage/'.$image->image_url)}}"
                                         alt="image">
                                </div>
                            @endforeach
                        </div>
                        <table class="table table-striped table-bordered" style="table-layout:fixed;">
                            <tr>
                                <th>Title</th>
                                <th>Value</th>
                            </tr>
                            <tbody>
                            <tr>
                                <td>Id</td>
                                <td>{{$post->id}}</td>
                            </tr>
                            <tr>
                                <td>User</td>
                                <td>{{$post->user->first_name}} {{$post->user->last_name}}</td>
                            </tr>
                            <tr>
                                <td>Title</td>
                                <td>{{$post->title}}</td>
                            </tr>
                            <tr>
                                <td>Description</td>
                                <td>{{$post->description}}</td>
                            </tr>
                            <tr>
                                <td>Category</td>
                                <td>{{$post->category->type}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
<link href="{{asset('limitless/global_assets/css/images.css')}}" rel="stylesheet" type="text/css">

<script>
    var slideIndex = 0;
    showSlides();

    function showSlides() {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex > slides.length) {
            slideIndex = 1
        }
        slides[slideIndex - 1].style.display = "block";
        setTimeout(showSlides, 2000); // Change image every 2 seconds
    }
</script>
