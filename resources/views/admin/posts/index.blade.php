@extends('admin.layouts.app')
@section('title',__('Posts'))
@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{__('Posts')}}</h4>
                    <a href="{{route('admin.posts.create')}}"
                       class="btn-sm btn-info block-page float-right">
                        {{__('Add Posts')}}
                    </a>
                </div>

                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">

                        @if($items->count() > 0)
                            <table class="table table-striped table-bordered" style="text-align: center;">
                                <thead>
                                <tr>
                                    <th>{{__('Image')}}</th>
                                    <th>{{__('user')}}</th>
                                    <th>{{__('Title')}}</th>
                                    <th>{{__('Description')}}</th>
                                    <th>{{__('Category Type')}}</th>
                                    <th>{{__('Status')}}</th>
                                    <th>{{__('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td>
                                            <img style="width: 200px; height: 150px;"
                                                 src="{{asset('storage/'.$item->postImages->first()['image_url'])}}"
                                                 alt=" image">
                                        </td>
                                        <td>{{$item->user->first_name}} {{$item->user->last_name}}</td>
                                        <td>{{$item->title}}</td>
                                        <td>{{$item->description}}</td>
                                        <td>{{$item->category->type}}</td>
                                        <td>{{$item->status == 1 ? __('Active') : __('Inactive')}}</td>
                                        <td>
                                             <span class="dropdown">
                                                 <button id="btnSearchDrop28"
                                                         type="button" data-toggle="dropdown"
                                                         aria-haspopup="true"
                                                         aria-expanded="true"
                                                         class="btn btn-primary dropdown-toggle dropdown-menu-right">
                                            <i class="icon-cog"></i>
                                                 </button>
                                                <span aria-labelledby="btnSearchDrop28"
                                                      class="dropdown-menu mt-1 dropdown-menu-left">
                                            <a href="{{route('admin.posts.edit',$item-> id)}}"
                                               class="dropdown-item block-page">
                                                <i class="icon-pencil"></i> {{__('Edit')}}
                                            </a>
                                                     <a href="{{route('admin.posts.show',$item-> id)}}"
                                                        class="dropdown-item block-page">
                                                <i class="icon-eye"></i> {{__('View')}}
                                            </a>
                                                  <form action="{{route('admin.posts.update', $item->id)}}"
                                                        method="post">
                                                         @csrf @method('put')
                                                      <input type="hidden" name="status"
                                                             value="{{ $item->status == 0 ?  1 : 0 }}">
                                                        <button href="#" class="dropdown-item block-page"><i
                                                                class="icon-check"></i>
                                                            {{ $item->status == 0 ?  __('Active') : __('Inactive')}}
                                                        </button>
                                                  </form>
                                                    <form method="post"
                                                          action="{{route('admin.posts.destroy',$item-> id)}}">
                                                    @csrf @method('DELETE')
                                                    <button class="dropdown-item block-page"><i class="icon-trash"></i>
                                                        {{__('Delete')}}
                                                    </button>
                                                    </form>
                                                </span>
                                             </span>
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>

                            {{$items->links()}}

                        @else
                            <h4 class="alert alert-danger text-center"> {{__('There is no items')}}</h4>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
