<div class="col-md-12">
    <div class="slideshow-container m-4">
        @foreach($post->postImages as $image)
            <div class="mySlides fade">
                <img style="width: 40%; height: 40%;"
                     src="{{asset('storage/'.$image['image_url'])}}"
                     alt="image">
            </div>
        @endforeach
    </div>
</div>

<link href="{{asset('limitless/global_assets/css/images.css')}}" rel="stylesheet" type="text/css">

<script>
    var slideIndex = 0;
    showSlides();

    function showSlides() {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex > slides.length) {
            slideIndex = 1
        }
        slides[slideIndex - 1].style.display = "block";
        setTimeout(showSlides, 2000); // Change image every 2 seconds
    }
</script>
