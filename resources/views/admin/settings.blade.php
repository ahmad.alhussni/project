@extends('admin.layouts.app')
@section('title',__('Settingss'))
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active"> {{__('Settings')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="basic-form-layouts">
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-form"> {{__('Settings')}}</h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis-v font-medium-3"></i></a>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <form class="form"
                                      action="{{route('admin.settings')}}"
                                      method="POST"
                                      enctype="multipart/form-data">
                                    @csrf

                                    <div class="form-body">

                                        <div class="row">

                                            @foreach($settings as $setting => $type)
                                                @foreach (config('app.locales') as $locale)
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group row">
                                                                    @if($type == "image")
                                                                        <label class="col-md-3 label-control"
                                                                               for="name">
                                                                            {{__($setting)}}
                                                                            @if(count(config('app.locales')) > 1)
                                                                                ({{__($locale)}})
                                                                            @endif
                                                                        </label>
                                                                        <div class="col-md-9">

                                                                            <img width="100%"
                                                                                 style="background: #f0f0f3;"
                                                                                 src="/storage/{{ App\Models\Setting::translate($setting,$locale) }}">
                                                                            <input
                                                                                type="file"
                                                                                id="{{$setting}}[{{$locale}}]"
                                                                                name="{{$setting}}[{{$locale}}]">
                                                                        </div>
                                                                    @elseif($type == "textarea")
                                                                        <label class="col-md-3 label-control"
                                                                               for="name">
                                                                            {{__($setting)}}
                                                                            @if(count(config('app.locales')) > 1)
                                                                                ({{__($locale)}})
                                                                            @endif
                                                                        </label>
                                                                        <div class="col-md-9">

                                                                        <textarea
                                                                            type="{{$type}}"
                                                                            id="{{$setting}}[{{$locale}}]"
                                                                            name="{{$setting}}[{{$locale}}]"
                                                                            class="form-control border-primary"
                                                                            placeholder="{{__($setting)}} @if(count(config('app.locales')) > 1) ({{__($locale)}}) @endif"
                                                                        >{{ App\Models\Setting::translate($setting,$locale) }}</textarea>
                                                                        </div>
                                                                    @elseif($type == "checkbox")
                                                                        @if($locale == "en")
                                                                            <label class="col-md-3 label-control"
                                                                                   for="name">
                                                                                {{__($setting)}}
                                                                            </label>
                                                                            <div class="col-md-9">

                                                                                <input
                                                                                    {{App\Models\Setting::translate($setting,$locale) == "on" ? "checked" : ""}}
                                                                                    type="{{$type}}"
                                                                                    id="{{$setting}}[{{$locale}}]"
                                                                                    name="{{$setting}}[{{$locale}}]"
                                                                                    class="border-primary"
                                                                                    placeholder="{{__($setting)}} @if(count(config('app.locales')) > 1) ({{__($locale)}}) @endif">
                                                                            </div>
                                                                        @endif
                                                                    @else
                                                                        @if(in_array($setting,['name','description','logo','admin_logo','footer_logo']))
                                                                            <label class="col-md-3 label-control"
                                                                                   for="name">
                                                                                {{__($setting)}}
                                                                                @if(count(config('app.locales')) > 1)
                                                                                    ({{__($locale)}})
                                                                                @endif
                                                                            </label>
                                                                            <div class="col-md-9">

                                                                                <input
                                                                                    type="{{$type}}"
                                                                                    id="{{$setting}}[{{$locale}}]"
                                                                                    name="{{$setting}}[{{$locale}}]"
                                                                                    class="form-control border-primary"
                                                                                    placeholder="{{__($setting)}} @if(count(config('app.locales')) > 1) ({{__($locale)}}) @endif"
                                                                                    value="{{ App\Models\Setting::translate($setting,$locale) }}">
                                                                            </div>
                                                                        @else
                                                                            @if($locale == "en")
                                                                                <label class="col-md-3 label-control"
                                                                                       for="name">
                                                                                    {{__($setting)}}
                                                                                </label>
                                                                                <div class="col-md-9">
                                                                                    <input
                                                                                        type="{{$type}}"
                                                                                        id="{{$setting}}[{{$locale}}]"
                                                                                        name="{{$setting}}[{{$locale}}]"
                                                                                        class="form-control border-primary"
                                                                                        placeholder="{{__($setting)}}"
                                                                                        value="{{ App\Models\Setting::translate($setting,$locale) }}">
                                                                                </div>
                                                                            @endif
                                                                        @endif
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endforeach

                                        </div>

                                    </div>

                                    <div class="form-actions">
                                        <button type="button" class="btn btn-warning mr-1 block-page"
                                                onclick="history.back();">
                                            <i class="icon-cross"></i> {{__('Cancel')}}
                                        </button>
                                        <button type="submit" class="btn btn-primary block-page">
                                            <i class="icon-check"></i> {{__('Submit')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
@endsection
