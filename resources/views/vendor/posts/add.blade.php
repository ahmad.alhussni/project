@extends('vendor.layouts.app')
@section('title',__('Posts'))
@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('vendor.home')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{route('vendor.posts.index')}}"> {{__('Posts')}}</a></li>
                        <li class="breadcrumb-item active"> {{__('Add Post')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="basic-form-layouts">
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-form"> {{__('Add Post')}}</h4>
                            <a class="heading-elements-toggle">
                                <i class="icon-ellipsis-v font-medium-3"></i></a>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <form class="form" action="{{route('vendor.posts.store')}}" method="POST"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label> {{__('Post image')}} </label>
                                            <label id="projectinput7" class="file center-block">
                                                <input type="file" id="images" name="images[]"
                                                       multiple="multiple" value="{{ old('images[]')}}">
                                                <span class="file-custom"></span>
                                            </label>
                                            @error('images[]')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"
                                                                   for="title">{{__('Post Title')}}</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="title"
                                                                       name="title" class="form-control border-primary"
                                                                       placeholder="{{__('Post Title')}}"
                                                                           value="{{old('title')}}">
                                                                @error('title')
                                                                <span
                                                                    class="text-danger">{{$message}}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" for="description">
                                                                {{__('Post description')}}</label>
                                                            <div class="col-md-9">
                                                                <textarea type="text" id="description"
                                                                          name="description"
                                                                          class="form-control border-primary"
                                                                          placeholder="{{__('Post description')}}">{{old('description')}}</textarea>
                                                                @error('description')
                                                                <span
                                                                    class="text-danger">{{$message}}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <p class="text-secondary">{{__("Select Category")}}</p>
                                                    <select class="select2-rtl form-control" name="category_id"
                                                            id="select2-rtl-multi">
                                                        @foreach($categories as $category)
                                                            <option
                                                                value="{{$category->id}}" {{ $category->id ==  old('category_id') ? 'selected' : ''}}>
                                                                {{$category->type}}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="button" class="btn btn-warning mr-1 block-page"
                                                onclick="history.back();">
                                            <i class="icon-cross"></i> {{__('Cancel')}}
                                        </button>
                                        <button type="submit" class="btn btn-primary block-page">
                                            <i class="icon-check"></i> {{__('Submit')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('js')
    <script src="//cdn.ckeditor.com/4.15.0/full/ckeditor.js"></script>
    <script>
        @foreach (config('app.locales') as $locale)
        CKEDITOR.replace('description-{{$locale}}', {
            rtl: true
        });
        @endforeach
    </script>
@endsection

{{--
   @foreach (config('app.locales') as $locale)
                                                <div class="col-md-6">
                                                    <div class="row">

                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 label-control"
                                                                       for="title-{{$locale}}">{{__('Post Title')}} @if(count(config('app.locales')) > 1)
                                                                        ({{__($locale)}}) @endif</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" id="title-{{$locale}}"
                                                                           name="title[{{$locale}}]"
                                                                           class="form-control border-primary"
                                                                           placeholder="{{__('Post Title')}} @if(count(config('app.locales')) > 1) ({{__($locale)}}) @endif"
                                                                           value="{{ old('title') ? old('title')[$locale] : '' }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 label-control"
                                                                       for="description-{{$locale}}">{{__('Post description')}} @if(count(config('app.locales')) > 1)
                                                                        ({{__($locale)}}) @endif</label>
                                                                <div class="col-md-9">
                                                                <textarea type="text" id="description-{{$locale}}"
                                                                          name="description[{{$locale}}]"
                                                                          class="form-control border-primary"
                                                                          placeholder="{{__('Post description')}} @if(count(config('app.locales')) > 1) ({{__($locale)}}) @endif">{{ old('description') ? old('description')[$locale] : '' }}</textarea>
                                                                    @error('description')
                                                                    <span
                                                                        class="text-danger">{{$message}}</span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            @endforeach
--}}
